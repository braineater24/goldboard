#include "Goldboard4.h"

Goldboard4 gb;

#define MOTOR_0	2
#define MOTOR_1	0
#define MOTOR_2	1


void fahren(int x, int y, int d);

int main()
{
	while(1)
	{	
		gb.motor[MOTOR_0].rotate(100);
		gb.motor[MOTOR_1].rotate(100);
		gb.motor[MOTOR_2].rotate(100);
		/*delay(3000);
		gb.motor[MOTOR_0].rotate(0);
		gb.motor[MOTOR_1].rotate(0);
		gb.motor[MOTOR_2].rotate(0);
		delay(3000);*/
	}
}




#define SIN60	0.8660254f
#define COS60	0.5f

/*
*/

void fahren(int x, int y, int d)
{
	int speed0 = COS60*y - SIN60*x + d;
	int speed1 = x + d;
	int speed2 = -COS60*y - SIN60*x + d;

	gb.motor[MOTOR_0].rotate(speed0);
	gb.motor[MOTOR_1].rotate(speed1);
	gb.motor[MOTOR_2].rotate(speed2);
}
	
