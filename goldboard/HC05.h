/**********************************************************************
@file       HC05.h

@brief      Contains the functions for the Goldboard4 bluetooth module extension

-----------------------------------------------------------------------
@author  2017 Alexander Ulbrich
         alexander.ulbrich@uni-ulm.de
-----------------------------------------------------------------------

@end
***********************************************************************/

#ifndef __HC05_H__
#define __HC05_H__


#include "Print.h"
#include "uart.h"
#include "config.h"

#define AT_RESET 	"AT+RESET"
#define AT_DEFAULT 	"AT+ORGL"
#define AT_NAME		"AT+NAME"
#define AT_PASSWORD	"AT+PSWD"
#define AT_ROLE		"AT+ROLE"
	#define SLAVE_ROLE 	"0"
	#define MASTER_ROLE "1"
#define ATINQUIER 	"AT+INQ"
#define AT_INIT 	"AT+INIT"
#define AT_STATE 	"AT+STATE"


#define HC05_SERIAL_ERROR	1
#define HC05_DEVICE_ERROR	2
#define HC05_LIB_ERROR		3
#define HC05_NO_ERROR		0
#define HC05_TIMEOUT_ERROR	4

#define AT_BUFFERSIZE		100

#define AT_STATE_UNINITALIZED		0
#define AT_STATE_INITIALIZED		1
#define AT_STATE_READY				2
#define AT_STATE_PAIRABLE			3
#define AT_STATE_PAIRED				4
#define AT_STATE_INQUIRING			5
#define AT_STATE_CONNECTING			6
#define AT_STATE_CONNECTED 			7
#define AT_STATE_DISCONNECTED 		8






class HC05 
{
//variables
public:
protected:
private:

//functions
public:
	HC05();
	uint8_t setPairingPassword(const char* Password);
	uint8_t setDeviceName(const char* deviceName);
	uint8_t factoryResetDevice();
	uint8_t inquierPairing();
	uint8_t getStatus();
	uint8_t setMode(const char* mode); //Master or Slave (TODO: Master mode)
	uint8_t init();

protected:
private:
	uint8_t getResponse();
	uint8_t getResponse(char* responseBuffer,int size);
	const char* deviceName;
	const char* pairingPassword;
	uint8_t state;


}; //HC05

#endif //__HC05_H__
