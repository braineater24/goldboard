/**********************************************************************
@file       HC05.cpp

@brief      Contains the functions for the Goldboard4 bluetooth module extension

-----------------------------------------------------------------------
@author  2017 Alexander Ulbrich
         alexander.ulbrich@uni-ulm.de
-----------------------------------------------------------------------
@History

1.00 28.01.2015 First working version

@end
***********************************************************************/

#include "HC05.h"
#include "uart.h"
#include "config.h"
#include "wiring_private.h"

HC05::HC05()
{
	uart_init(UART_BAUD_SELECT(38400UL,F_CPU));	

	//getStatus();
	//if(state == AT_STATE_PAIRED || state == AT_STATE_CONNECTED)
	//	return;
	init();
	setPairingPassword("6666");
	setMode(SLAVE_ROLE);
	setDeviceName("Us_ROBOT");
	inquierPairing();
}

uint8_t HC05::init()
{
	uart_puts(AT_INIT);
	uart_puts("\r\n");
	if(getResponse())
		state = AT_STATE_UNINITALIZED; //TODO: ERROR(17)
	else
		state = AT_STATE_INITIALIZED;
	return 0;
}

uint8_t HC05::getResponse()
{
	return getResponse(0,0);
}

uint8_t HC05::getResponse(char* responseBuffer, int bufferSize)
{

	delay(100); //TODO: is this needed?????

	uint32_t currentTime = millis();
	char buffer[AT_BUFFERSIZE];
	int index = 0;

	while(1)
	{
		int x = uart_getc();
		uint8_t c = x&0xff;
		uint8_t error = x>>8;
		if(error!=UART_NO_DATA && error) //keine Daten ist kein Fehler!
			return HC05_SERIAL_ERROR;
		if(millis()-currentTime>5000)	//timeout 5s
			return HC05_TIMEOUT_ERROR;
		if(index == AT_BUFFERSIZE)		//bufferoverflow protection
			return HC05_LIB_ERROR;
		buffer[index] = c;
		index++;
		if(c == '\n')					//end of response
			break;
	}


	//TODO: better parsing
	if(buffer[0] == 'O' && buffer[1] == 'K')
	{
		return HC05_NO_ERROR;
	}

	if(buffer[0] == 'E' && buffer[1] == 'R')
	{
		return HC05_DEVICE_ERROR; //TODO more info
	}

	if(buffer[0] == 'P' && buffer[5]=='D') //paired
	{
		state = AT_STATE_PAIRED;
		return HC05_NO_ERROR;
	}
	if(buffer[0] == 'C' && buffer[8]=='D') //connected
	{
		state = AT_STATE_CONNECTED;
		return HC05_NO_ERROR;
	}
	return HC05_LIB_ERROR;
}


uint8_t HC05::setPairingPassword(const char* Password)
{
	uart_puts(AT_PASSWORD);
	uart_puts("=");
	uart_puts(Password);
	uart_puts("\r\n");
	return getResponse();
}


uint8_t HC05::setDeviceName(const char* deviceName)
{
	uart_puts(AT_NAME);
	uart_puts("=");
	uart_puts(deviceName);
	uart_puts("\r\n");
	return getResponse();
}

uint8_t HC05::factoryResetDevice() //TODO: not realy good...
{
	uint8_t ret;
	uart_puts(AT_RESET);
	uart_puts("\r\n");
	if((ret=getResponse()));
	delay(100);
	uart_puts(AT_DEFAULT);
	uart_puts("\r\n");
	if((ret=getResponse()));
	delay(100);
	uart_puts(AT_INIT);
	uart_puts("\r\n");
	return getResponse();
}

uint8_t HC05::inquierPairing()
{
	uart_puts(ATINQUIER);
	uart_puts("\r\n");
	return getResponse();
}

uint8_t HC05::setMode(const char* mode)
{

	uart_puts(AT_ROLE);
	uart_puts("=");
	uart_puts(mode);
	uart_puts("\r\n");
	return getResponse();
}

uint8_t HC05::getStatus()
{
	uart_puts(AT_STATE);
	uart_puts("?");
	uart_puts("\r\n");
	return getResponse();
}