#include "Goldboard4.h"
Goldboard4 gb;

/*
Bei Fehlern:
-Werte: fors in reset_values auskommentieren
-Schuss: größer statt kleiner in der while
*/

/*
ToDo:
- Testen: - Schuss
	  - Ballsuche
*/

/* MOTOREN */
#define VL 0
#define VR 1
#define HL 2
#define HR 3

#define SPEED 255
int motorspeed[4] = {-1,-1,-1,-1};
int speed = 200;

/*Ball*/
#define NO_BALL -1
#define BALL_VALUE 20
int sensorwert_ball[8] = {-2,-2,-2,-2,-2,-2,-2,-2};
int sensor_ballkuhle = 0;
int groesster_Sensorwert = NO_BALL;
int ball_nummer = NO_BALL;
bool ballnah = false;
bool ballda = false;

/*Kompass*/	//0 liegt bei 90°, wenn 128 = vorne
#define compass_Soll 128
CMPS03 cmp;
int compass = 0;	// 0...255

/*Bodenlichtsensoren*/
#define BODENvorne 0
#define BODENlinks 1
#define BODENhinten 2
#define BODENrechts 3 
int bodenlichtsensoren[4] = {0,0,0,0};
int bodenlichtsensorWert[4] = {100,100,100,100};
int bodenlichtsensorWertWeiss[4] = {100,100,100,100};

/*für Auswertung d Bodenlichtsensoren*/
#define BODENvorne_innen -55
#define BODENlinks_innen -56
#define BODENhinten_innen -57
#define BODENrechts_innen -58


/*Position Linie*/
#define SPIELFELD             	-10
#define LINIErechtsSchwarz	90
#define LINIElinksSchwarz	270
#define LINIEhintenSchwarz	0
#define LINIEvorneSchwarz	180
#define LINIEvornelinksSchwarz	225
#define LINIEhintenlinksSchwarz	315
#define LINIEvornerechtsSchwarz	135
#define LINIEhintenrechtsSchwarz 45
#define LINIElinksrechtsSchwarz	-2
#define LINIEvornehintenSchwarz	-3
#define HOCHGEHOBEN		-4 //Verwendung fraglich
#define LINIEhlrSchwarz         -5
#define LINIEvlrSchwarz		-6
#define LINIEvhlSchwarz		-7
#define LINIEvhrSchwarz		-8

#define LINIErechtsWeiss	-90
#define LINIElinksWeiss		-270
#define LINIEhintenWeiss	222
#define LINIEvorneWeiss		-180
#define LINIEvornelinksWeiss	-225
#define LINIEhintenlinksWeiss	-315
#define LINIEvornerechtsWeiss	-135
#define LINIEhintenrechtsWeiss	-45
#define LINIElinksrechtsWeiss	2
#define LINIEvornehintenweiss	3
#define LINIEhlrWeiss      	5
#define LINIEvlrWeiss		6
#define LINIEvhlWeiss		7
#define LINIEvhrWeiss		8

int boden = SPIELFELD;

/*Bodenauswertung*/
unsigned long letzter_wert_start = 0;
unsigned long vorletzter_wert_start = 0;
int letzter_wert = -1;
int vorletzter_wert = -1;


/*Ultraschall*/
SRF08 ultraschallhinten;
SRF08 ultraschallvorne;
#define vorne 0
#define hinten 1
int sonar[2] = {0,0};
//ultraschall.changeAddress(2);  ändert sonar adresse
//delay(50);
//ultraschall.getValueCM();		 gibt wert in cm
//ultraschall.init(0);			 initalisierung; 0 = ID
	
/*Digitalsensoren*/
int buttons[4] = {0,0,0,0};
#define ECKE_DREHEN 0
#define ECKE_FAHREN 1
#define OHNE_ECKE 2
int spielweise = ECKE_DREHEN;
int taktik = ECKE_DREHEN;

unsigned long ballda_start = 0;
	
void fahren(int richt, int speed, int drall)
{
	switch(richt)
	{
		case 0:					//vorne
			motorspeed[VL] = -1;
			motorspeed[VR] =  1;
			motorspeed[HL] = -1;
			motorspeed[HR] =  1;
		break;
		case 45:				//vorne links
			motorspeed[VL] =  0;
			motorspeed[VR] =  1;
			motorspeed[HL] = -1;
			motorspeed[HR] =  0;			
		break;
		case 90:          //links
			motorspeed[VL] =  1;
			motorspeed[VR] =  1;
			motorspeed[HL] = -1;
			motorspeed[HR] = -1;
		break;
		case 135:        //hinten links
			motorspeed[VL] =  1;
			motorspeed[VR] =  0;
			motorspeed[HL] =  0;
			motorspeed[HR] = -1;
		break;
		case 180:        //rückwärts
			motorspeed[VL] =  1;
			motorspeed[VR] = -1;
			motorspeed[HL] =  1;
			motorspeed[HR] = -1;
		break;
		case 225:       //hinten rechts
			motorspeed[VL] =  0;
			motorspeed[VR] = -1;
			motorspeed[HL] =  1;
			motorspeed[HR] =  0;
		break;
		case 270:        //rechts
			motorspeed[VL] = -1;
			motorspeed[VR] = -1;
			motorspeed[HL] =  1;
			motorspeed[HR] =  1;
		break;
		case 315:        //vorne rechts
			motorspeed[VL] = -1;
			motorspeed[VR] =  0;
			motorspeed[HL] =  0;
			motorspeed[HR] =  1;
		break;
			
		default:
		break;
	}
	
/**************************************************
		MOTOREN

	Motor0		Motor1

	Motor2		Motor3

	roboter dreht sich nach links wenn alle Motoren vorwärts drehen

*************************************************/
	
	motorspeed[VL] = speed*motorspeed[VL] + drall;
	motorspeed[VR] = speed*motorspeed[VR] + drall;
	motorspeed[HL] = speed*motorspeed[HL] + drall;
	motorspeed[HR] = speed*motorspeed[HR] + drall;
	
	for (int i = 0; i < 4; i++) 
	{
		if (motorspeed[i] > 255)
			motorspeed[i] = 255;
		else if (motorspeed[i] < -255)
			motorspeed[i] = -255;
	}
	
	gb.motor[0].rotate(motorspeed[VL]);
	gb.motor[1].rotate(motorspeed[VR]);
	gb.motor[2].rotate(motorspeed[HL]);
	gb.motor[3].rotate(motorspeed[HR]);
	
}

void fahren (int richt, int speed)
{
	int drall = ((compass-128)*(1,2));	//Rotation //UUU: komma?????
	drall = 0;
	fahren(richt, speed, drall);
}

void ballwerte () 
{
	sensorwert_ball[0] = gb.getPWMPulsedLight(0) +5;  //UUU: +5????
	sensorwert_ball[1] = gb.getPWMPulsedLight(1);
	sensorwert_ball[2] = gb.getPWMPulsedLight(2);
	sensorwert_ball[3] = gb.getPWMPulsedLight(3);
	sensorwert_ball[4] = gb.getPWMPulsedLight(4);
	sensorwert_ball[5] = gb.getPWMPulsedLight(5);
	sensorwert_ball[6] = gb.getPWMPulsedLight(6);
	sensorwert_ball[7] = gb.getPWMPulsedLight(7);		

	/**************************
	Ballsensoren am Roboter:
			0
		1		7
	2				6
		3		5
			4
	**************************/
}

unsigned long timer_ball_nummer = 0;

int ballMaxSensor(int data[], int datasize, int ballValue) //gebe ein array[] ein, gebe die größe des array ein, gebe den untersten wert ein
{
	int posOfHighestBallSensor = NO_BALL;

	for(int pass = 0; pass < datasize; pass++)
	{
		if(data[pass] > ballValue)
		{
			ballValue = data[pass];
			posOfHighestBallSensor = pass;
		}
	}
	
	if ((posOfHighestBallSensor == 1 && ball_nummer == 7) ||(posOfHighestBallSensor == 7 && ball_nummer == 1))
	{
		posOfHighestBallSensor == 0;
	}
	
	/*if (posOfHighestBallSensor != ball_nummer)
	{
		if (millis() - timer_ball_nummer < 200 && posOfHighestBallSensor != ball_nummer)
		{
			posOfHighestBallSensor = ball_nummer;
		}
		else if (posOfHighestBallSensor == ball_nummer)
		{
			posOfHighestBallSensor = ball_nummer;
		}
		else if (posOfHighestBallSensor != ball_nummer && millis() - timer_ball_nummer > 199)
		{
			timer_ball_nummer = millis();
		}
	}*/
	return posOfHighestBallSensor; //gibt die position des Sensors mit dem höchsten wert zurück
}

/*  //Falls ballMaxSensor nicht funktioniert, hier neue BallMaxSensor, vgl. void Vergleichen()
ballMaxSensor()
{
	if ()
}
*/

int ballMaxSensor()
{
	return ballMaxSensor(sensorwert_ball, 8, BALL_VALUE);
}

void ballumrunden (int ball_position, int speed)
{
	int winkel = -2;
	switch (ball_position)
	{
		case 0:
			winkel = 0;
		break;
		
		case 1:
			winkel = 90;
		break;
		
		case 2:
			winkel = 180;
		break;
		
		case 3:
			winkel = 180;
		break;
		case 4:
			winkel = 270;
		break;
		case 5:
			winkel = 180;
		break;
		case 6:
			winkel = 180;
		break;
		case 7:
			winkel = 270;
		break;
		
		default:
			winkel = -1; //UUU: Was passiert bei diesem Fall??? 
		break;
	}
	if(winkel != -2) //UUU: Sinnlos ohne else Teil
		fahren(winkel,SPEED);
}

unsigned long timer_value_start = 0;
unsigned long timer_value = 0;

void Ausgeben()
{
	timer_value = millis();
	timer_value_start = 0;
	
	/*while (timer_value - timer_value_start > 500) //UUU: so wäre es besser gewesen.
	{
		 //SERIAL_PRINT("Compass: ");
		 SERIAL_PRINTLN(compass);
		 timer_value_start = millis();
	}*/
	SERIAL_PRINTLN(compass);
	delay(600); //UUU: vorsicht Das beeinflusst die Schleifen dauer

	//SERIAL_PRINTLN();
	//delay(500);
}

void drehen(int speed)
{
 gb.motor[0].rotate(speed);	
 gb.motor[1].rotate(speed);	
 gb.motor[2].rotate(speed);	
 gb.motor[3].rotate(speed);	   
}

unsigned long Zeit_Schuss_Start = 0;
unsigned long Zeit_Schuss = 0;
unsigned long Schuss_Start = 0;

void Schuss()
{    
	
	 gb.setPower(1,0);
	 /*Zeit_Schuss_Start = millis();
	 while ((millis() - Zeit_Schuss_Start) < 25) //falls es nicht klappt, statt größer, kleiner einfügen
	 {
		gb.setPower(1,1);
	 }
	 gb.setPower(1,0);*/                                                                                                                                                                                               
	 

	/*if ((millis() - Schuss_Start) >50)
	{
		gb.setPower(1,0);
		gb.setPower(1,1);
		Schuss_Start = millis();
		delay(24);
		gb.setPower(1,0);		
	}
	else
	{
		gb.setPower(1,0);
	}*/
	
	gb.setPower(1,0);
	gb.setPower(1,1);
	delay(25);
	gb.setPower(1,0);

}

void reset_values()
{
	ultraschallhinten.init(1);      //initalisierung; 0 = ID
	ultraschallvorne.init(0);
	//cmp.init();	//bei Störungen auskommentieren; Kompassinitialisierung
	//gb.initLED(0);
	gb.initLED(1);
	Zeit_Schuss = millis();
	Zeit_Schuss_Start = millis();
	timer_value = millis();
	timer_value_start = millis();
	ballda_start = millis();
  	for (int i=0; i<20; i++) //UUU: hat die Schleife einen Sinn? Wenn ja ist es vll ein Hardware Problem!!!!
 	{
		cmp.setAs128Degree();
    }
	cmp.setAs128Degree();
	/*for (int i = 0; i < 8; i++)	//falls bei den Ball-, Schalter-, Taster- oder Bodenwerten fehler auftreten, die unteren beiden fors auskommentieren
	{
		sensorwert_ball[i] = 0;
	}
	for(int i = 0; i < 4; i++)                                                                   
	{
		buttons[i] = 0;
		bodenlichtsensoren[i] = 0;
	}*/
}

void compass_auslesen()
{
	compass = cmp.getValue();	
}


#define MOTOR_AUS 1
#define MOTOR_AUS_AUSGEBEN 2
#define SPIEL 3
bool ecke_drehen = 0;
int motor_zustand = SPIEL;

void DigitalValues()
{
	/*for (int i = 0; i < 4; i++)
	{
		buttons[i] = gb.getDigital(i); 
	}*/
	
	buttons[0] = gb.getDigital(0);
	buttons[1] = gb.getDigital(1);
	buttons[2] = gb.getDigital(2);
	buttons[3] = gb.getDigital(3);

/******************************
	buttons[0] = Schalter1
	buttons[1] = Schalter2
	buttons[2] = Taster1
	buttons[3] = Taster2
******************************/

	if ((!buttons[0]) && (!buttons[1]))
	{
		taktik = OHNE_ECKE;
	}
	else if (buttons[0])
	{
		taktik = ECKE_DREHEN;
	}
	else if (buttons[1])
	{
		taktik = ECKE_FAHREN;
	}
	
}

void ultraschall_wert()
{
	sonar[hinten] = ultraschallhinten.getValueCM();
	sonar[vorne] = ultraschallvorne.getValueCM();
}

void ball_value()
{
	sensor_ballkuhle = gb.getAnalog(7);
}

void ballauswertung()
{
	if ((ball_nummer == 0) && (sensor_ballkuhle > 50))
	{
		ballnah = true;
	}
	else if ((sensor_ballkuhle > 80) && (ball_nummer == 0)) //UUU: ballda tritt nie ein. if und else if gehören vertauscht.
	{
		ballda == true;
	}
}
void bodenlichtsensoren_Auslesen()
{
	bodenlichtsensoren[BODENvorne] = gb.getAnalog(BODENvorne);
	bodenlichtsensoren[BODENlinks] = gb.getAnalog(BODENlinks);
	bodenlichtsensoren[BODENhinten] = gb.getAnalog(BODENhinten);
	bodenlichtsensoren[BODENrechts] = gb.getAnalog(BODENrechts);

	/******************************
	Pinbelegung und Positionen der Bodenlichtsensoren:
			0
		1		3
			2
	******************************/	
}

int bodenlichtsensoren_Auswerten()
{
	int a = SPIELFELD;	// a als int, die später zurückgegeben wird
		
	bool bodenrichtung[4] = {0,0,0,0};
	bool bodenrichtungWeiss[4] = {0,0,0,0};

	if (bodenlichtsensoren[BODENvorne] > bodenlichtsensorWert[BODENvorne])
	{
		bodenrichtung[BODENvorne_innen] = true;		
	}

	if (bodenlichtsensoren[BODENrechts] < bodenlichtsensorWert[BODENrechts])
	{
		bodenrichtung[BODENrechts_innen] = true;	
	}

	if (bodenlichtsensoren[BODENlinks] < bodenlichtsensorWert[BODENlinks])
	{
		bodenrichtung[BODENlinks_innen] = true;	
	}

	if (bodenlichtsensoren[BODENhinten_innen] < bodenlichtsensorWert[BODENhinten_innen])
	{
		bodenrichtung[BODENhinten_innen] = true;	
	}


	if (bodenlichtsensoren[BODENvorne] > bodenlichtsensorWertWeiss[BODENvorne])
	{
		bodenrichtungWeiss[BODENvorne_innen] = true;		
	}

	if (bodenlichtsensoren[BODENrechts] < bodenlichtsensorWertWeiss[BODENrechts])
	{
		bodenrichtungWeiss[BODENrechts_innen] = true;	
	}

	if (bodenlichtsensoren[BODENlinks] < bodenlichtsensorWertWeiss[BODENlinks])
	{
		bodenrichtungWeiss[BODENlinks_innen] = true;	
	}

	if (bodenlichtsensoren[BODENhinten_innen] < bodenlichtsensorWertWeiss[BODENhinten_innen])
	{
		bodenrichtungWeiss[BODENhinten_innen] = true;	
	}


	if (!(bodenrichtung[BODENvorne_innen])&&!(bodenrichtung[BODENhinten_innen])&&!(bodenrichtung[BODENrechts_innen])&&!(bodenrichtung[BODENlinks_innen]))
		a = SPIELFELD;
	else if (!(bodenrichtungWeiss[BODENvorne_innen])&&!(bodenrichtungWeiss[BODENhinten_innen])&&!(bodenrichtungWeiss[BODENrechts_innen])&&!(bodenrichtungWeiss[BODENlinks_innen]))
		a = SPIELFELD;
	else if ((bodenrichtung[BODENvorne_innen])&&!(bodenrichtung[BODENhinten_innen])&&!(bodenrichtung[BODENrechts_innen])&&!(bodenrichtung[BODENlinks_innen]))
		a = LINIEvorneSchwarz;
	else if (!(bodenrichtung[BODENvorne_innen])&&(bodenrichtung[BODENhinten_innen])&&!(bodenrichtung[BODENrechts_innen])&&!(bodenrichtung[BODENlinks_innen]))
		a = LINIEhintenSchwarz; 
	else if (!(bodenrichtung[BODENvorne_innen])&&!(bodenrichtung[BODENhinten_innen])&&(bodenrichtung[BODENrechts_innen])&&!(bodenrichtung[BODENlinks_innen]))
		a = LINIErechtsSchwarz;
	else if (!(bodenrichtung[BODENvorne_innen])&&!(bodenrichtung[BODENhinten_innen])&&!(bodenrichtung[BODENrechts_innen])&&(bodenrichtung[BODENlinks_innen]))
		a = LINIElinksSchwarz;
	else if ((bodenrichtung[BODENvorne_innen])&&!(bodenrichtung[BODENhinten_innen])&&!(bodenrichtung[BODENrechts_innen])&&(bodenrichtung[BODENlinks_innen]))
		a = LINIEvornelinksSchwarz;
	else if ((bodenrichtung[BODENvorne_innen])&&!(bodenrichtung[BODENhinten_innen])&&(bodenrichtung[BODENrechts_innen])&&!(bodenrichtung[BODENlinks_innen]))
		a = LINIEvornerechtsSchwarz;
	else if (!(bodenrichtung[BODENvorne_innen])&&(bodenrichtung[BODENhinten_innen])&&!(bodenrichtung[BODENrechts_innen])&&(bodenrichtung[BODENlinks_innen]))
		a = LINIEhintenlinksSchwarz;
	else if (!(bodenrichtung[BODENvorne_innen])&&(bodenrichtung[BODENhinten_innen])&&(bodenrichtung[BODENrechts_innen])&&!(bodenrichtung[BODENlinks_innen]))
		a = LINIEhintenrechtsSchwarz;
	else if ((bodenrichtung[BODENvorne_innen])&&(bodenrichtung[BODENhinten_innen])&&!(bodenrichtung[BODENrechts_innen])&&!(bodenrichtung[BODENlinks_innen]))
		a = LINIEvornehintenSchwarz;
	else if (!(bodenrichtung[BODENvorne_innen])&&!(bodenrichtung[BODENhinten_innen])&&(bodenrichtung[BODENrechts_innen])&&(bodenrichtung[BODENlinks_innen]))
		a = LINIElinksrechtsSchwarz;
	else if (!(bodenrichtung[BODENvorne_innen])&&(bodenrichtung[BODENhinten_innen])&&(bodenrichtung[BODENrechts_innen])&&(bodenrichtung[BODENlinks_innen]))
		a = LINIEhlrSchwarz;
	else if ((bodenrichtung[BODENvorne_innen])&&!(bodenrichtung[BODENhinten_innen])&&(bodenrichtung[BODENrechts_innen])&&(bodenrichtung[BODENlinks_innen]))
		a = LINIEvlrSchwarz;
	else if ((bodenrichtung[BODENvorne_innen])&&(bodenrichtung[BODENhinten_innen])&&!(bodenrichtung[BODENrechts_innen])&&(bodenrichtung[BODENlinks_innen]))
		a = LINIEvhlSchwarz;
	else if ((bodenrichtung[BODENvorne_innen])&&(bodenrichtung[BODENhinten_innen])&&(bodenrichtung[BODENrechts_innen])&&!(bodenrichtung[BODENlinks_innen]))
		a = LINIEvhrSchwarz;	
	else if ((bodenrichtungWeiss[BODENvorne_innen])&&!(bodenrichtungWeiss[BODENhinten_innen])&&!(bodenrichtungWeiss[BODENrechts_innen])&&!(bodenrichtungWeiss[BODENlinks_innen]))
		a = LINIEvorneWeiss;
	else if (!(bodenrichtungWeiss[BODENvorne_innen])&&(bodenrichtungWeiss[BODENhinten_innen])&&!(bodenrichtungWeiss[BODENrechts_innen])&&!(bodenrichtungWeiss[BODENlinks_innen]))
		a = LINIEhintenWeiss; 
	else if (!(bodenrichtungWeiss[BODENvorne_innen])&&!(bodenrichtungWeiss[BODENhinten_innen])&&(bodenrichtungWeiss[BODENrechts_innen])&&!(bodenrichtungWeiss[BODENlinks_innen]))
		a = LINIErechtsWeiss;
	else if (!(bodenrichtungWeiss[BODENvorne_innen])&&!(bodenrichtungWeiss[BODENhinten_innen])&&!(bodenrichtungWeiss[BODENrechts_innen])&&(bodenrichtungWeiss[BODENlinks_innen]))
		a = LINIElinksWeiss;
	else if ((bodenrichtungWeiss[BODENvorne_innen])&&!(bodenrichtungWeiss[BODENhinten_innen])&&!(bodenrichtungWeiss[BODENrechts_innen])&&(bodenrichtungWeiss[BODENlinks_innen]))
		a = LINIEvornelinksWeiss;
	else if ((bodenrichtungWeiss[BODENvorne_innen])&&!(bodenrichtungWeiss[BODENhinten_innen])&&(bodenrichtungWeiss[BODENrechts_innen])&&!(bodenrichtungWeiss[BODENlinks_innen]))
		a = LINIEvornerechtsWeiss;
	else if (!(bodenrichtungWeiss[BODENvorne_innen])&&(bodenrichtungWeiss[BODENhinten_innen])&&!(bodenrichtungWeiss[BODENrechts_innen])&&(bodenrichtungWeiss[BODENlinks_innen]))
		a = LINIEhintenlinksWeiss;
	else if (!(bodenrichtungWeiss[BODENvorne_innen])&&(bodenrichtungWeiss[BODENhinten_innen])&&(bodenrichtungWeiss[BODENrechts_innen])&&!(bodenrichtungWeiss[BODENlinks_innen]))
		a = LINIEhintenrechtsWeiss;
	else if ((bodenrichtungWeiss[BODENvorne_innen])&&(bodenrichtungWeiss[BODENhinten_innen])&&!(bodenrichtungWeiss[BODENrechts_innen])&&!(bodenrichtungWeiss[BODENlinks_innen]))
		//a = LINIEvornehintenWeiss;
		a = SPIELFELD;
	else if (!(bodenrichtungWeiss[BODENvorne_innen])&&!(bodenrichtungWeiss[BODENhinten_innen])&&(bodenrichtungWeiss[BODENrechts_innen])&&(bodenrichtungWeiss[BODENlinks_innen]))
		a = LINIElinksrechtsWeiss;
	else if (!(bodenrichtungWeiss[BODENvorne_innen])&&(bodenrichtungWeiss[BODENhinten_innen])&&(bodenrichtungWeiss[BODENrechts_innen])&&(bodenrichtungWeiss[BODENlinks_innen]))
		a = LINIEhlrWeiss;
	else if ((bodenrichtungWeiss[BODENvorne_innen])&&!(bodenrichtungWeiss[BODENhinten_innen])&&(bodenrichtungWeiss[BODENrechts_innen])&&(bodenrichtungWeiss[BODENlinks_innen]))
		a = LINIEvlrWeiss;
	else if ((bodenrichtungWeiss[BODENvorne_innen])&&(bodenrichtungWeiss[BODENhinten_innen])&&!(bodenrichtungWeiss[BODENrechts_innen])&&(bodenrichtungWeiss[BODENlinks_innen]))
		a = LINIEvhlWeiss;
	else if ((bodenrichtungWeiss[BODENvorne_innen])&&(bodenrichtungWeiss[BODENhinten_innen])&&(bodenrichtungWeiss[BODENrechts_innen])&&!(bodenrichtungWeiss[BODENlinks_innen]))
		a = LINIEvhrWeiss;	

   return a;
}

void auswertung_werte()
{
		//bodenlichtsensoren_Auslesen(); //Auslesen von Werten der Bodenlichtsensoren
		//boden = bodenlichtsensoren_Auswerten(); //Auswerten von Bodenlichtsensoren
		
		compass_auslesen(); //Auslesen von Magnetfeldsensor
		
		DigitalValues(); //Auslesen von Tastern und Schaltern //UUU: muss man im Spiel nicht auslesen. 

		ultraschall_wert(); //Ultraschall auslesen (je nach verwendungszweck)

		ballwerte(); //Ballsensorewerte auslesen
	   	ball_nummer = ballMaxSensor(); //Auswerten der Werte
		if (ball_nummer != NO_BALL) //wenn Ball da ist //UUU: gut Idee ;-)
		{
			ball_value(); //Ballkuhle auslesen
			ballauswertung();	
		}


}

int main(void)
{

	
	reset_values(); //kontrollieren, ob etwas auskommentiert ist
	
	while(true)
	{
		auswertung_werte();
		
		if (buttons[2] && !buttons[3])
		{
			Schuss();
		}
		else if (!buttons[2] && buttons[3])
		{
			break;
		}
		else if (buttons[2] && buttons[3])
		{
			reset_values();			
		}
		else if (!buttons[2] && !buttons[3] && buttons[0])
		{
			Ausgeben();
		}
	}
		
	while(1)
	{
		auswertung_werte();

		if (ballMaxSensor() == 0 && sensorwert_ball[0] > 70)
		{
			if (sonar[vorne] < 15)
			{
				fahren(0,255); //UUU: unnötig
				if (compass<128) //UUU: wird nie 100% Funktionieren!!! Besser mit Ultraschall zur Seite.
				{
					drehen(-200);
				}
				else
				{
					drehen(200);
				}	
				Schuss();
			}
			else
			{
				Schuss();
				fahren(0,255);
			}
		}
		else if (ballMaxSensor() != NO_BALL)
		{
			ballumrunden(ballMaxSensor(),255);
		}
		else
		{
			
			if (sonar[hinten] > 20)
			{
				fahren(180,155);
			}
			else
			{
				fahren(0,0,0);
			}
		}
	}
}
